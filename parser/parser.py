'''
Abstract parser interface.
'''

# Imports

from abc import ABCmeta, abstractmethod


# Class

class Parser(object):
    '''
    Abstract parser class.
    '''

    __metaclass__ = ABCmeta

    def parse(self, filename):
        '''
        Parses some input file and returns the processed result.

        * filename -- the path to the file to be parsed
        
        '''
        pass

