'''
Parses Athena file formats.

NOTE: this file has been copied from pyathena and has to be refactored. Do not
use it as a template for other parsers.

'''

# Imports

import csv
import os

import numpy as np

from parser import Parser


# Classes

class TabParser(Parser):
    '''
    Parses a tabular dump (.tab file) as it can be created by Athena.

    Dumps are typically stored in a folder enumerated according to some format, like so

    Problem.0000.tab
    Problem.0001.tab
    ...
    Problem.1234.tab

    '''

    def __init__(self, path, problem, enum_fmt="%04d", extension="tab"):
        '''
        Constructor.

        * path     -- the path where the tabular dumps are stored
        * problem  -- the problem name used to name the tabular dumps
        * enum_fmt -- enumeration format string
        * extension -- file extension

        '''

        # Set parameters
        self.path = path
        self.problem = problem
        self.enum_fmt = enum_fmt
        self.extension = extension

    def filename(self, dump_index):
        '''
        Gets filename for given index.

        * dump_index -- index of the file

        '''
        return self.path + self.problem + '.' + self.enum_fmt%(dump_index) + '.' + self.extension

    def parse(self, filename):
        '''
        Private method that actually does the work of parsing the file using
        the csv module.

        * filename -- the name of the tabular dump to be parsed

        '''
        nx1, nx2 = 1, 1
        
        print 'Reading:', filename

        with open(filename, 'r') as tab_file:
            
            tab_reader = csv.reader(tab_file, delimiter=' ', skipinitialspace=True)
            data = []
            
            for line in tab_reader:
                
                # Read meta-information in the header
                if line[0] == '#':
                    if line[1] == 'Nx1':
                        nx1 = int(line[3])
                    if line[1] == 'Nx2':
                        nx2 = int(line[3])
                    continue

                # Try to read the data from the file
                x, y, rho, u, v, p = [0.0]*6
                try:
                    x = float(line[2])
                    y = float(line[3])
                    rho = float(line[4])
                    u = float(line[5])
                    v = float(line[6])
                    p = float(line[8])
                except ValueError:
                    # Handle some bug in Athena output uglily
                    print "Warning: misshaped data in %s; inserting zero.\n\t%s\n\t=> %s" % (filename, line, str((x, y, rho, u, v, p)))

                # Append values to list
                data.append((x, y, rho, u, v, p))
        
        # Create an empty structured array and fill it with the data read
        struct_data = np.array(data, dtype = {'names': ['x', 'y', 'rho', 'u', 'v', 'P'], 'formats': ['f8'] * 6})
        struct_data = np.reshape(struct_data, (nx2, nx1))

        return struct_data


class TabParserMPI(AthenaTabFileParser):
    '''
    Specialized version of TabParser that parses output from
    MPI-parallelized runs.

    These runs typically store their output in different directories called
    id0, id1 ... (until number of parallel processes is reached).
    
    TODO: Explain the nomenclature for different SMR levels...

    '''
    
    def __init__(self, path, problem, enum_fmt="%04d", extension="tab", structure=(1,1)):
        '''
        Constructor.

        * path     -- the path where the tabular dumps are stored
        * problem  -- the problem name used to name the tabular dumps
        * enum_fmt -- enumeration format string
        * extension -- file extension
        * structure -- tuple of integers representing the structure of the
            domain decomposition due to MPI usage

        '''
        # Base class constructor
        TabParser.__init__(self, path, problem, enum_fmt, extension)

        # Determine number of processes run
        self.nproc = sum(1 for name in os.listdir(self.path) if name[:2] == 'id')
        if self.nproc != structure[0]*structure[1]:
            raise ValueError("Structure must match number of processes!")
        print 'Found %d output directories from MPI run' % (self.nproc)
        
        # Set structure parameter
        self.structure = structure
        

    def parse_multiple(self, dump_index, smr_level=1):
        '''
        Parses tabular dumps of the outputs from all subprocesses and merges
        them into a single list of numpy arrays containing the relevant
        quantities.

        * dump_index -- temporal index
        * smr_level  -- level of static mesh refinement (SMR)

        '''
        
        # Create empty array
        whole_data = []
        
        # Iterate over all process output directories
        for proc_index in range(self.nproc):
            single_data = self.parse(self.filename(dump_index, proc_index, smr_level))
            whole_data.append(single_data)

        # Number of subdomains along x/y axis
        Nx, Ny = self.structure

        # Concatenate along x-axis
        rows = []
        for ny in range(Ny):
            row = np.concatenate([whole_data[nx] for nx in range(ny*Nx, (ny+1)*Nx)], axis = 1)
            rows.append(row)

        # Concatenate along y-axis
        total = np.concatenate(rows, axis = 0)

        # Return them
        return total

    def filename(self, dump_index, proc_index, smr_level):
        '''
        Returns the filename of a tabular dump for given temporal and process
        index.
* wrap simulation domain into bigger low-resolution domain to get the boundaries far from the actual problem
        
        * dump_index -- temporal index of the file
        * proc_index -- index of process
        * smr_level  -- level of static mesh refinement (SMR)

        '''
        # Setup format string
        fmt_string = '{0}/id{1}'
        if smr_level > 0:
            fmt_string += '/lev{5}'
        fmt_string += '/{2}'
        if proc_index > 0:
            fmt_string += '-id{1}'
        if smr_level > 0:
            fmt_string += '-lev{5}'
        fmt_string += '.{3}.{4}'

        # Build file name with it
        fname = fmt_string.format(self.path, proc_index, self.problem, self.enum_fmt % (dump_index,), self.extension, smr_level)
        return fname

