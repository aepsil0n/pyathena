'''
This module provides the core functionality required to setup athena, configure
it properly and run a problem.
'''

import subprocess
import shutil
import os
import errno
import logging


class AthenaError(Exception):
    '''
    Exception class to handle error codes returned from the Athena
    configure/make/run cycle.

    '''

    def __init__(self, value):
        self.value = value

    def __str__(self):
        return repr(self.value)


class Athena(object):
    '''
    This class provides an interface to Athena.
    '''

    __pyathena_problem_name = 'pyathena_custom'
    __input_file = 'tst/pyathena.param'

    def __init__(self, athena_path="athena/"):
        '''
        Constructor.

        * athena_path -- The path to the athena directory.

        >>> ath = Athena('/some/dir/', 8)
        >>> ath.athena_path
        '/some/dir/'
        >>> ath.nproc
        8
        >>> ath.packages
        {}
        >>> ath.features
        {}

        '''

        self.athena_path = athena_path
        self.packages = {}
        self.features = {}

        # Default values for parallelism
        self.nproc = 1
        self.parallel_compile = False
        self.parallel_run = False

    def set_feature(self, feature, status):
        '''
        Sets a feature to a certain boolean state (enabled or disabled).

        * feature -- string that corresponds to the Athena feature
        * state   -- either True (=enabled) or False (=disabled)

        >>> ath = Athena()
        >>> ath.set_feature('viscosity', False)
        >>> ath.features['viscosity']
        False

        '''
        self.features[feature] = bool(status)

    def enable_feature(self, feature):
        '''
        Enables a feature.

        * feature -- the feature to be enabled

        >>> ath = Athena()
        >>> ath.enable_feature('conduction')
        >>> ath.features['conduction']
        True

        '''
        # TODO: add optional check, whether argument is actually an Athena feature
        self.set_feature(feature, True)

    def disable_feature(self, feature):
        '''
        Disables a feature.

        * feature -- the feature to be disabled

        >>> ath = Athena()
        >>> ath.disable_feature('resistivity')
        >>> ath.features['resistivity']
        False

        '''
        self.set_feature(feature, False)

    def set_package(self, package, value):
        '''
        Sets an argument for an optional package.

        * package -- the package to configure
        * value   -- the value to be passed as argument to the package

        In this example Athena is configured to assume an isothermal equation
        of state:

        >>> ath = Athena()
        >>> ath.set_package('eos', 'isothermal')
        >>> ath.packages['eos']
        'isothermal'
        >>> ath.set_package('problem', 'someproblem.c')
        ValueError

        '''
        self.packages[package] = str(value)


    def clean(self):
        '''
        Cleans up products from earlier compilations of Athena.
        '''
        exit_code = subprocess.call('make clean', cwd = self.athena_path, shell = True)
        if exit_code:
            logging.warning('cleaning Athena directory not possible')

    def setup_problem(self, cproblem=None):
        '''
        Configures and compiles the Athena code for a specific problem

        * cproblem -- the file that contains the c source code for the problem;
            if None it is assumed that a built-in problem generator is used,
            which has been configured via
            set_package('problem', 'somebuiltinproblem')

        '''

        # Setup configure options
        # TODO: implement!
        option_string = ''

        # Setup features
        for feature in self.features:
            if self.features[feature] is True:
                option_string += ' --enable-%s' % (feature)
            elif self.features[feature] is False:
                option_string += ' --disable-%s' % (feature)

        # Setup packages
        for package in self.packages:
            option_string += ' --with-%s=%s' % (package, self.packages[package])

        # Copy C file to Athena's problem directory
        if cproblem is not None:
            shutil.copyfile(cproblem, self.athena_path + 'src/prob/%s.c' % (self.__pyathena_problem_name))
            option_string += ' --with-problem=%s' % (self.__pyathena_problem_name)

        # Run configure script in Athena directory
        cfg_command = './configure %s' % (option_string)
        print '$', cfg_command
        exit_code = subprocess.call(cfg_command, cwd = self.athena_path, shell = True)
        if exit_code:
            raise AthenaError('problem configuration failed')

        # Compile the problem
        make_all_cmd = 'make ' + ('-j %d ' % (self.nproc) if self.parallel_compile else '') + 'all'
        print '$', make_all_cmd
        exit_code = subprocess.call(make_all_cmd, cwd = self.athena_path, shell = True)
        if exit_code:
            raise AthenaError('compiling of the problem setup failed')

    def setup_configfile(self, cfg_file):
        '''
        Sets up the configuration file.  Basically this method copies the
        argument into the Athena folder.

        * cfg_file -- the configuration file containing input parameters

        TODO: generate input file automagically

        '''
        shutil.copyfile(cfg_file, self.athena_path + self.__input_file)

    def setup_parallelism(self, nproc, parallel_compile = True, parallel_run = True):
        '''
        Sets up parallelism.

        * nproc            -- number of processes to be initiated to compile
            and run the code
        * parallel_compile -- whether the compilation of the code should be
            parallelized
        * parallel_run     -- whether running the code should be parallelized;
            as a side-effect this flag automatically enables the MPI feature. 

        '''

        self.nproc = nproc
        self.parallel_compile = parallel_compile
        self.parallel_run = parallel_run
        self.set_feature('mpi', parallel_run)


    def run(self, output_dir = None):
        '''
        Runs Athena as configured.

        * output_dir -- optional output directory; Athena will be run from this
            directory, thereby dumping any output files here.  If None, Athena
            will simply be run in its root directory, given in athena_path.

        '''

        # Path parameters
        executable_path = ''
        inputfile_path = ''
        working_dir = ''

        # Check if output directory was supplied
        if output_dir is None:
            # Execute Athena in its own root directory
            executable_path = 'bin/athena'
            inputfile_path = self.__input_file
            working_dir = self.athena_path
        else:
            # Make sure the directory exists
            try:
                os.makedirs(output_dir)
            except OSError as exception:
                if exception.errno != errno.EEXIST:
                    raise

            # Execute Athena in the output directory
            executable_path = os.path.abspath(self.athena_path + '/bin/athena')
            inputfile_path = os.path.abspath(self.athena_path + self.__input_file)
            working_dir = output_dir

        # Set MPI command
        mpi_cmd = 'mpirun -np %d' % (self.nproc) if self.parallel_run else ''

        # Actually run athena
        exit_code = subprocess.call('%s %s -i %s' % (mpi_cmd, executable_path, inputfile_path), cwd = working_dir, shell = True)
        if exit_code:
            raise AthenaError('Athena run returned error')

