'''
Plots PlanetDisk scenario from Athena output.
'''

# Imports

from vis.density import AnimatedDensityField
from parser.athena import AthenaTabFileParserMPI


# Test calling routine

if __name__ == '__main__':
    
    # Some parameters
    path = '/home/ebopp/ita-ebopp/PlanetDisk/'
    problem = 'PlanetDisk'

    # Create tab file parser
    parser = AthenaTabFileParserMPI(path, problem)
    parser.nproc = 2 # Particularly evil hack

    # Create animation
    animation = AnimatedDensityField(parser, vmin = 0.98, vmax = 1.02)

    # Animate and save it
    animation.save(path + 'movie.mp4', fps = 30)

