'''
Script to test the Athena handler.
'''

# Imports

from handler.athena import Athena


# Script

if __name__ == '__main__':
    
    # Create Athena instance
    athena = Athena('athena4.1/')

    # Set configuration options
    athena.set_package('gas', 'hydro')
    athena.set_package('flux', 'roe')
    athena.set_package('eos', 'adiabatic')

    # Clean folder and compile the problem
    athena.clean()
    athena.setup_problem('example/planet_disk_box.c')
    athena.setup_configfile('example/athinput.planet_disk_box')
    athena.run(output_dir = 'output/PlanetDisk/')

