'''
Plots random data to test visualization.
'''

# Imports

import numpy as np

from pyathena.vis.density import AnimatedDensityField


# Test calling routine

if __name__ == '__main__':
    
    def random_data(i):
        x, y = np.meshgrid(np.linspace(0, 10, 100), np.linspace(0, 10, 100))
        c = np.cos(0.04*y*y + 0.03*x*y - 0.8*i)
        u = np.sin(x + 0.3*i) + 0.3*(y)
        v = np.cos(y - 0.7*i) + 0.4*(x)
        return x, y, c, u, v

    # Create animation
    animation = AnimatedDensityField(random_data, start = 0, end = 100, vmin = 0.0, vmax = 1.0)
    animation.initialize()

    # Animate and save it
    animation.save('test.mp4', fps = 25)

