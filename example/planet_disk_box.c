#include "copyright.h"
/*============================================================================*/
/*! \file planet_disk_box.c
 *  \brief Problem generator for a planet in a disk.
 * 
 * Planet in protoplanetary disk on a simple 2D Euclidean grid.
 * 
 */
/*============================================================================*/

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include "defs.h"
#include "athena.h"
#include "globals.h"
#include "prototypes.h"

/* Pre-declarations */

void disk_inflow_left_x1(GridS *pGrid);


/* Static problem variables */

static Real planet_radius, stream_velocity, gas_density, noise_level, gas_temperature;


/* Helper functions */

int inside_planet(Real x1, Real x2, Real planet_radius)
{
    return (x1*x1+x2*x2 < planet_radius*planet_radius);
}

/*----------------------------------------------------------------------------*/
/* problem:   */

void problem(DomainS *pDomain)
{
    // Declare variables
    GridS *pGrid = pDomain->Grid;
    int i,is,ie,j,js,je,ks,nx1,nx2;
    Real x1,x2,x3;
    Real tmp_density;

    // Parse problem parameters from input file
    planet_radius   = par_getd("problem", "planet_radius");
    stream_velocity = par_getd("problem", "stream_velocity");
    gas_density     = par_getd("problem", "gas_density");
    noise_level     = par_getd("problem", "noise_level");
    gas_temperature = par_getd("problem", "gas_temperature");

    // Get grid start/end indices
    is = pGrid->is;
    ie = pGrid->ie;
    js = pGrid->js;
    je = pGrid->je;
    ks = pGrid->ks;

    // Setup conserved quantities along the grid  
    for (j=js; j<=je; j++) {
        for (i=is; i<=ie; i++) {
            cc_pos(pGrid, i, j, ks, &x1, &x2, &x3);


            if (inside_planet(x1, x2, planet_radius)) {
                pGrid->U[ks][j][i].d = gas_density;
                pGrid->U[ks][j][i].M1 = 0.0;
                pGrid->U[ks][j][i].M2 = 0.0;
                pGrid->U[ks][j][i].M3 = 0.0;
                pGrid->U[ks][j][i].E = gas_temperature;
            } else {
                tmp_density = gas_density*(1.0 + noise_level*(rand()%10000 - 5000)/10000.0);
                pGrid->U[ks][j][i].d = tmp_density;
                pGrid->U[ks][j][i].M1 = stream_velocity;
                pGrid->U[ks][j][i].M2 = 0.0;
                pGrid->U[ks][j][i].M3 = 0.0;
                pGrid->U[ks][j][i].E = gas_temperature + 0.5*tmp_density*stream_velocity*stream_velocity;
            }

        }
    }

    // Register custom boundary condition
    bvals_mhd_fun(pDomain, left_x1, disk_inflow_left_x1);

    return;
}

/*==============================================================================
 * PROBLEM USER FUNCTIONS:
 * problem_write_restart() - writes problem-specific user data to restart files
 * problem_read_restart()  - reads problem-specific user data from restart files
 * get_usr_expr()          - sets pointer to expression for special output data
 * get_usr_out_fun()       - returns a user defined output function pointer
 * get_usr_par_prop()      - returns a user defined particle selection function
 * Userwork_in_loop        - problem specific work IN     main loop
 * Userwork_after_loop     - problem specific work AFTER  main loop
 *----------------------------------------------------------------------------*/

void problem_write_restart(MeshS *pM, FILE *fp)
{
    return;
}

void problem_read_restart(MeshS *pM, FILE *fp)
{
    // Read parameters from input file
    planet_radius   = par_getd("problem", "planet_radius");
    stream_velocity = par_getd("problem", "stream_velocity");
    gas_density     = par_getd("problem", "gas_density");
    noise_level     = par_getd("problem", "noise_level");
    gas_temperature = par_getd("problem", "gas_temperature");

    return;
}

ConsFun_t get_usr_expr(const char *expr)
{
    return NULL;
}

VOutFun_t get_usr_out_fun(const char *name) {
    return NULL;
}

void Userwork_in_loop(MeshS *pM)
{
    GridS *pGrid;
    pGrid = pM->Domain[0][0].Grid;

    int i, is, ie, j, js, je, ks, nx1, nx2;
    Real x1, x2, x3;
    Real absx, n1, n2, upar;

    is = pGrid->is;
    ie = pGrid->ie;
    js = pGrid->js;
    je = pGrid->je;
    ks = pGrid->ks;

    // Manipulate grid points in a circle that is the planet
    for (j=js; j<=je; j++) {
        for (i=is; i<=ie; i++) {

            // Determine position on the grid and skip loop if not inside planet
            cc_pos(pGrid, i, j, ks, &x1, &x2, &x3);
            if (! inside_planet(x1, x2, planet_radius))
                continue;

            // Calculate tangential velocity component and normal vector at surface
            // Doesn't work as expected
            /*
               absx = sqrt(x1*x1 + x2*x2);
               n1 = x1/absx;
               n2 = x2/absx;
               upar = pGrid->U[ks][j][i].M2 * n1 - pGrid->U[ks][j][i].M1 * n2;
             */

            // Reset quantities inside the planet
            pGrid->U[ks][j][i].d = gas_density;
            pGrid->U[ks][j][i].M1 = 0.0;
            pGrid->U[ks][j][i].M2 = 0.0;
            pGrid->U[ks][j][i].M3 = 0.0;
            pGrid->U[ks][j][i].E = gas_temperature;
        }
    }
}

void Userwork_after_loop(MeshS *pM)
{
}

/**
 * Custom boundary conditions
 */

void disk_inflow_left_x1(GridS *pGrid)
{
    int i, is, j, js, je, ks;

    is = pGrid->is;
    js = pGrid->js;
    je = pGrid->je;
    ks = pGrid->ks;

    for (j = js; j <= je; j++) {
        for (i = 1; i <= nghost; i++) {
            pGrid->U[ks][j][is-i].d = gas_density;
            pGrid->U[ks][j][is-i].M1 = stream_velocity;
            pGrid->U[ks][j][is-i].M2 = 0.0;
            pGrid->U[ks][j][is-i].M3 = 0.0;
            pGrid->U[ks][j][is-i].E = gas_temperature + 0.5*gas_density*stream_velocity*stream_velocity;
        }
    }
}

